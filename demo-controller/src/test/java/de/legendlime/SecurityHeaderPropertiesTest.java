package de.legendlime;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import de.legendlime.filter.SecurityHeaderProperties;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import jakarta.inject.Inject;

@MicronautTest
class SecurityHeaderPropertiesTest {

	@Inject
	SecurityHeaderProperties props;
	
	@Test
	void checkForConfigs() {
		Assertions.assertNotEquals(props.getCacheControl(), null);
		Assertions.assertNotEquals(props.getContentSecurityPolicy(), null);
		Assertions.assertNotEquals(props.getStrictTransportSecurity(), null);
		Assertions.assertNotEquals(props.getContentTypeOptions(), null);
		Assertions.assertNotEquals(props.getFrameOptions(), null);
		Assertions.assertNotEquals(props.getXssProtection(), null);
	}
	
}
