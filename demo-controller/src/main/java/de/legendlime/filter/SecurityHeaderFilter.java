/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.filter;


import org.reactivestreams.Publisher;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Filter;
import io.micronaut.http.filter.HttpServerFilter;
import io.micronaut.http.filter.ServerFilterChain;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;
import jakarta.inject.Inject;

/**
 * Security Headers Servlet filter
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 */

@Filter(value = "/**")
public class SecurityHeaderFilter implements HttpServerFilter {
	
	@Inject
	private SecurityHeaderProperties props;
		
	@Override
	public Publisher<MutableHttpResponse<?>> doFilter(HttpRequest<?> request,
			ServerFilterChain chain) {
        return Flowable.fromCallable(() -> {
            return true;
        }).subscribeOn(Schedulers.io()).switchMap(a -> chain.proceed(request)).doOnNext(res -> {
            res.getHeaders().add("Strict-Transport-Security",props.getStrictTransportSecurity());
            res.getHeaders().add("X-Content-Type-Options",props.getContentTypeOptions());
            res.getHeaders().add("X-Frame-Options",props.getFrameOptions());
            res.getHeaders().add("X-XSS-Protection",props.getXssProtection());
            res.getHeaders().add("Content-Security-Policy", props.getContentSecurityPolicy());
            res.getHeaders().add("Cache-Control",props.getCacheControl());
        });
	}

}