/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.filter;


import org.reactivestreams.Publisher;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Filter;
import io.micronaut.http.filter.HttpServerFilter;
import io.micronaut.http.filter.ServerFilterChain;
import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;


/**
 * 
 * CORS filter is disabled here hence activated via
 * application.yml settings file
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 *
 */

@Filter(value = "/not-active")
public class CorsFilter implements HttpServerFilter {

	@Override
	public Publisher<MutableHttpResponse<?>> doFilter(HttpRequest<?> request,
			ServerFilterChain chain) {
        return Flowable.fromCallable(() -> {
            return true;
        }).subscribeOn(Schedulers.io()).switchMap(a -> chain.proceed(request)).doOnNext(res -> {
            res.getHeaders().add("Access-Control-Allow-Credentials","true");
            res.getHeaders().add("Access-Control-Allow-Methods","*");
            res.getHeaders().add("Access-Control-Allow-Origin","*");
            res.getHeaders().add("Access-Control-Allow-Headers","*");
        });
	}

}