/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.filter;

import io.micronaut.context.annotation.ConfigurationProperties;

/**
 * Security headers properties are taken from application.yml
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 *
 */

@ConfigurationProperties("legendlime.securityHeaders")
public class SecurityHeaderProperties {

	private String contentTypeOptions;

	private String frameOptions;

	private String xssProtection;

	private String contentSecurityPolicy;

	private String cacheControl;

	private String strictTransportSecurity;


	public SecurityHeaderProperties() {
	}


	public String getContentTypeOptions() {
		return contentTypeOptions;
	}


	public void setContentTypeOptions(String contentTypeOptions) {
		this.contentTypeOptions = contentTypeOptions;
	}


	public String getFrameOptions() {
		return frameOptions;
	}


	public void setFrameOptions(String frameOptions) {
		this.frameOptions = frameOptions;
	}


	public String getXssProtection() {
		return xssProtection;
	}


	public void setXssProtection(String xssProtection) {
		this.xssProtection = xssProtection;
	}


	public String getContentSecurityPolicy() {
		return contentSecurityPolicy;
	}

	public void setContentSecurityPolicy(String contentSecurityPolicy) {
		this.contentSecurityPolicy = contentSecurityPolicy;
	}

	public String getCacheControl() {
		return cacheControl;
	}

	public void setCacheControl(String cacheControl) {
		this.cacheControl = cacheControl;
	}

	public String getStrictTransportSecurity() {
		return strictTransportSecurity;
	}

	public void setStrictTransportSecurity(String strictTransportSecurity) {
		this.strictTransportSecurity = strictTransportSecurity;
	}

}
