/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.filter;

import java.text.ParseException;
import java.util.Optional;

import org.reactivestreams.Publisher;

import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;

import de.legendlime.api.SignatureException;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Filter;
import io.micronaut.http.filter.HttpServerFilter;
import io.micronaut.http.filter.ServerFilterChain;
import io.micronaut.http.filter.ServerFilterPhase;

/**
 * Extract the users full name from the access token
 * to be included into the document signature
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 *
 */

@Filter(Filter.MATCH_ALL_PATTERN)
public class SignerNameFilter implements HttpServerFilter {

    public static final String SIGNER_ATTRIBUTE_NAME = "signerName";

    @Override
    public Publisher<MutableHttpResponse<?>> doFilter(final HttpRequest<?> request, final ServerFilterChain chain) {
    	
    	String givenName = "";
    	String familyName = "";
    	
    	Optional<String> token = request.getHeaders().getAuthorization();
    	if (token.isPresent()) {
    		try {
				JWT accessToken = JWTParser.parse(token.get().split(" ")[1]);
				givenName = (String) accessToken.getJWTClaimsSet().getClaim("given_name");
				familyName = (String) accessToken.getJWTClaimsSet().getClaim("family_name");
				
			} catch (ParseException e) {
				throw new SignatureException("Could not parse access token", e);
			}
    	}
        return chain.proceed(request.setAttribute(SIGNER_ATTRIBUTE_NAME, givenName + " " + familyName));
    }

    @Override
    public int getOrder() {
        return ServerFilterPhase.SECURITY.order();
    }

}
