/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.dto;


import io.micronaut.serde.annotation.Serdeable;
import io.micronaut.serde.annotation.Serdeable.Serializable;

/**
 * Data transfer object with signature response
 * result JSON data object
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 * 
 * TODO: adjust DTO to final structure
 */

@Serdeable
@Serializable
public class ResultDTO {

	private String result;
	private String pdfFileName;
	private String signerName;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getPdfFileName() {
		return pdfFileName;
	}

	public void setPdfFileName(String pdfFileName) {
		this.pdfFileName = pdfFileName;
	}

	public String getSignerName() {
		return signerName;
	}

	public void setSignerName(String signerName) {
		this.signerName = signerName;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pdfFileName == null) ? 0 : pdfFileName.hashCode());
		result = prime * result + ((signerName == null) ? 0 : signerName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ResultDTO other = (ResultDTO) obj;
		if (pdfFileName == null) {
			if (other.pdfFileName != null)
				return false;
		} else if (!pdfFileName.equals(other.pdfFileName))
			return false;
		if (signerName == null) {
			if (other.signerName != null)
				return false;
		} else if (!signerName.equals(other.signerName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ResultDTO [pdfFileName=" + pdfFileName + ", signerName=" + signerName + "]";
	}

}
