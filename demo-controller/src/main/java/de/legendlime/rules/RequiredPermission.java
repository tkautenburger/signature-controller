/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.rules;

/**
 * RequiredPermission annotation takes the claim, client id,
 * and role pattern to enforce permission validation for
 * incoming requests on the REST controller
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 *
 */

public @interface RequiredPermission {

    /**
     * "realm" if realm access role and resource if "resource" access role
     * @return claim
     */
    String claim();

    /**
     * The client id for resource access type roles
     * @return client
     */
    String client();

    /**
     * The functional role required, e.g. SPI-Approver, without OU components
     * @return role
     */
    String role();
}
