/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.rules;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.micronaut.core.annotation.AnnotationValue;
import io.micronaut.http.HttpRequest;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.security.rules.SecurityRuleResult;
import io.micronaut.web.router.MethodBasedRouteMatch;
import io.micronaut.web.router.RouteMatch;
import io.reactivex.Flowable;
import jakarta.inject.Singleton;

/**
 * Custom security rule to enforce specific role settings
 * in the JWT access token for incoming REST requests
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 *
 */

@Singleton
public class SpiRoleSecurityRule implements SecurityRule {
	
	private final static String REALM_ACCESS = "realm";
	private final static String REALM_ACCESS_CLAIM = "realm_access";
	private final static String RESOURCE_ACCESS = "resource";
	private final static String RESOURCE_ACCESS_CLAIM = "resource_access";
	private final static String ROLES_CLAIM = "roles";
	
	private final Logger LOG = LoggerFactory.getLogger(SpiRoleSecurityRule.class);

	@SuppressWarnings("unchecked")
	@Override
	public Publisher<SecurityRuleResult> check(HttpRequest<?> request, RouteMatch<?> routeMatch,
			Authentication authentication) {
		
		if (authentication == null) {
			LOG.warn("no access token present or token expired.");
			return Flowable.just(SecurityRuleResult.REJECTED); 
		}

		if (routeMatch instanceof MethodBasedRouteMatch) {
			MethodBasedRouteMatch<?, ?> methodBasedRouteMatch = ((MethodBasedRouteMatch<?, ?>) routeMatch);
			if (methodBasedRouteMatch.hasAnnotation(RequiredPermission.class)) {
				AnnotationValue<RequiredPermission> requiredPermissionAnnotation = methodBasedRouteMatch.getAnnotation(RequiredPermission.class);
				
				Optional<String> claim = requiredPermissionAnnotation.stringValue("claim");
				Optional<String> client = requiredPermissionAnnotation.stringValue("client");
				Optional<String> functionalRole = requiredPermissionAnnotation.stringValue("role");
				
				Map<String, Object> map;
				Collection<String> roles = null;
				
				if (claim.isPresent() && claim.get().equals(RESOURCE_ACCESS)) {
					LOG.debug("Using resource-level role assignments for permissions");
					map = (Map<String, Object>) authentication.getAttributes().get(RESOURCE_ACCESS_CLAIM);
					if (map != null && client.isPresent()) {
						Map<String, Object> resource = (Map<String, Object>) map.get(client.get());
						roles = (Collection<String>) resource.get(ROLES_CLAIM);
						LOG.debug("Got effective roles from resource access: {}", roles);
					}
				} else if (claim.isPresent() && claim.get().equals(REALM_ACCESS)) {
					LOG.debug("Using realm-level role assignments for permissions");
					map = (Map<String, Object>) authentication.getAttributes().get(REALM_ACCESS_CLAIM);
					if (map != null) {
						roles = (Collection<String>) map.get(ROLES_CLAIM);
						LOG.debug("Got effective roles from realm access: {}", roles);
					}
				}
				if (roles != null && functionalRole.isPresent()) {
					Optional<String> role = roles.stream().filter(r -> r.startsWith(functionalRole.get())).findFirst();
					if (role.isPresent()) {
						LOG.info("Found role matching role pattern: {}. Grant access.", role.get());
						return Flowable.just(SecurityRuleResult.ALLOWED);
					}
				}
			}
		}
		LOG.info("Could not find any matching roles for pattern. Reject access.");
		return Flowable.just(SecurityRuleResult.REJECTED);
	}

}
