/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.api;

import io.micronaut.context.annotation.Requires;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.http.hateoas.Link;
import io.micronaut.http.server.exceptions.ExceptionHandler;
import jakarta.inject.Singleton;

/**
 * Signature exception handler that prepares an
 * exception response for failed REST requests
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 *
 */

@Produces
@Singleton 
@Requires(classes = {SignatureException.class, ExceptionHandler.class})
public class SignatureExceptionHandler implements ExceptionHandler<SignatureException, HttpResponse<?>> {

	@SuppressWarnings("rawtypes")
	@Override
	public HttpResponse<?> handle(HttpRequest request, SignatureException exception) {
		JsonError error = new JsonError(exception.getMessage());
		error.link(Link.SELF, Link.of(request.getUri()));
		return HttpResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).body(error);
	}

}
