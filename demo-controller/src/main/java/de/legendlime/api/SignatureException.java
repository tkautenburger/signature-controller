/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.api;

/**
 * Signature exception as runtime exception to be responded
 * in REST requests if something went wrong
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 *
 */

public class SignatureException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SignatureException() {
		super();
	}

	public SignatureException(String message, Throwable cause) {
		super(message, cause);
	}

	public SignatureException(String message) {
		super(message);
	}
}
