/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.api;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;

import javax.validation.Valid;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.DocumentException;

import de.legendlime.dto.ActionDTO;
import de.legendlime.dto.ResultDTO;
import de.legendlime.filter.SignerNameFilter;
import de.legendlime.rules.RequiredPermission;
import de.legendlime.service.SignatureService;
import io.micronaut.core.version.annotation.Version;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Consumes;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.RequestAttribute;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;

/**
 * SignatureController is the REST server for incoming signature requests from
 * client. Incoming requests must have a valid JWT access token with the
 * respective roles set in the token.
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 * 
 */

@Controller("/api")
@RequiredPermission(claim = "${SPI_SIGN_ROLE_CLAIM:resource}", client = "${SPI_SIGN_ROLE_PROVIDER_CLIENT:player-ui}", role = "${SPI_SIGN_ROLE_PATTERN:SPI-Approver}")
public class SignatureController {

	private final Logger LOG = LoggerFactory.getLogger(SignatureController.class);
	private final SignatureService service;

	public SignatureController(SignatureService service) {
		this.service = service;
	}

	/**
	 * TODO: This is just for testing purposes and the endpoint must be deleted for
	 * production environments
	 * 
	 * @param principal Java Principal with username
	 * @return username from JWT token as principal
	 */
	@Version("1")
	@Get("/user")
	@Produces(MediaType.APPLICATION_JSON)
	@ExecuteOn(TaskExecutors.IO)
	HttpResponse<?> hello(Principal principal) {
		ResultDTO result = new ResultDTO();
		result.setResult(principal.getName());

		if (principal.getName() == null) {
			LOG.error("principal name not found");
			throw new SignatureException("principal name not found");
		}
		LOG.info("returned principal name");
		return HttpResponse.status(HttpStatus.OK).body(result);
	}

	/**
	 * Sign PDF document referenced in request body and return the signed PDF
	 * document in the result JSON
	 * 
	 * @param action JSON structure from request body
	 * @return JSON structure with response results from signature operation
	 * 
	 * TODO: establish proper exception handling
	 */
	@Version("1")
	@Post("/sign")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@ExecuteOn(TaskExecutors.IO)
	HttpResponse<?> sign(@Body @Valid ActionDTO action,
			@RequestAttribute(SignerNameFilter.SIGNER_ATTRIBUTE_NAME) final String signerName) {
		ResultDTO result = null;

		try {
			result = service.signDocument(action, signerName);
			if (result != null) {
				result.setResult("OK");
				result.setSignerName(signerName);
				LOG.info("action successfully performed");
			}
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateExpiredException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateNotYetValidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OperatorCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
			throw new SignatureException("Error processing PDF document", e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new SignatureException("I/O Error", e);
		}
		return HttpResponse.status(HttpStatus.OK).body(result);
	}
}
