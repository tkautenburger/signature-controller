/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.service;

import java.io.IOException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;

import org.bouncycastle.cms.CMSException;
import org.bouncycastle.operator.OperatorCreationException;

import de.legendlime.dto.ActionDTO;
import de.legendlime.dto.ResultDTO;

/**
 * Service interface for signature service
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 *
 */
public interface SignatureService {
	
	ResultDTO signDocument(ActionDTO action, String signerName) throws KeyStoreException, UnrecoverableKeyException,
			CertificateExpiredException, CertificateNotYetValidException, CertificateParsingException,
			NoSuchAlgorithmException, CertificateException, IOException, CMSException, OperatorCreationException;

}
