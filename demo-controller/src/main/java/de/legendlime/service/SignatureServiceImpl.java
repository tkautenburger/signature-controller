/**
 * Copyright 2022 SPI Deutschland GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.legendlime.service;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bouncycastle.asn1.x509.KeyPurposeId;
import org.bouncycastle.cert.jcajce.JcaCertStore;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.jcajce.JcaSignerInfoGeneratorBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfDate;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfSignature;
import com.lowagie.text.pdf.PdfSignatureAppearance;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfString;

import de.legendlime.dto.ActionDTO;
import de.legendlime.dto.ResultDTO;
import jakarta.inject.Singleton;

/**
 * Signature Service Implementation Uses LibreOffice PDF library to perform a
 * PDF document signature
 * 
 * @author Thomas Kautenburger
 * @version 1.0.0
 *
 */

@Singleton
public class SignatureServiceImpl implements SignatureService {

	protected static final Logger LOG = LoggerFactory.getLogger(SignatureService.class);

	private final static String PIN = "123456";
	private final static int PADDING_SIZE = 8192;

	private PrivateKey privateKey;
	private Certificate[] certificateChain;
	private boolean externalSigning;

	@Override
	public ResultDTO signDocument(ActionDTO action, String signerName)
			throws KeyStoreException, UnrecoverableKeyException, CertificateExpiredException,
			CertificateNotYetValidException, CertificateParsingException, NoSuchAlgorithmException,
			CertificateException, IOException, CMSException, OperatorCreationException {

		KeyStore keystore = KeyStore.getInstance("PKCS12");
		keystore.load(this.getClass().getClassLoader().getResourceAsStream("sign_keystore.p12"), PIN.toCharArray());

		Enumeration<String> aliases = keystore.aliases();
		String alias;
		Certificate cert = null;
		Certificate[] certChain = null;
		while (cert == null && aliases.hasMoreElements()) {
			alias = aliases.nextElement();
			LOG.debug("Found keystore with alias {}", alias);
			setPrivateKey((PrivateKey) keystore.getKey(alias, PIN.toCharArray()));
			certChain = keystore.getCertificateChain(alias);
			if (certChain != null) {
				setCertificateChain(certChain);
				cert = certChain[0];
				LOG.debug("Got certificate {}", cert.toString());
				if (cert instanceof X509Certificate) {
					((X509Certificate) cert).checkValidity();
					checkCertificateUsage((X509Certificate) cert);
				}
			}
		}
		if (cert == null)
			throw new IOException("Could not find certificate");

		// from here on we have a valid private key and a certificate from the keystore
		try (InputStream is = new FileInputStream(action.getPdfFileName());
				OutputStream os = new FileOutputStream(action.getNewPdfFileName());
				ByteArrayOutputStream baos = new ByteArrayOutputStream()) {

			CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
			ContentSigner sha1Signer = new JcaContentSignerBuilder("SHA256WithRSA").build(privateKey);
			gen.addSignerInfoGenerator(
					new JcaSignerInfoGeneratorBuilder(new JcaDigestCalculatorProviderBuilder().build())
							.build(sha1Signer, (X509Certificate) cert));
			gen.addCertificates(new JcaCertStore(Arrays.asList(getCertificateChain())));
			Calendar signDate = Calendar.getInstance();
		
			PdfReader reader = new PdfReader(is);
			PdfStamper stp = PdfStamper.createSignature(reader, baos, '\0', null, true);
			stp.setEnforcedModificationDate(signDate);

			PdfSignatureAppearance sap = stp.getSignatureAppearance();
			sap.setCrypto(privateKey, certChain, null, null);
			sap.setVisibleSignature(new Rectangle(380,10, 580, 110), reader.getNumberOfPages(), signerName);

			PdfSignature dic = new PdfSignature(PdfName.ADOBE_PPKLITE, PdfName.ADBE_PKCS7_DETACHED);
			dic.setDate(new PdfDate(signDate));
			dic.setContact(signerName);

			sap.setCryptoDictionary(dic);
			sap.setReason(action.getReason());

			Map<PdfName, Integer> exc = new HashMap<>();
			exc.put(PdfName.CONTENTS, PADDING_SIZE * 2 + 2);
			sap.preClose(exc);

			CMSProcessableInputStream msg = new CMSProcessableInputStream(sap.getRangeStream());
			CMSSignedData signedData = gen.generate(msg, false);
			byte[] rawSignedData = signedData.getEncoded();
			byte[] paddedSignature = new byte[PADDING_SIZE];
			System.arraycopy(rawSignedData, 0, paddedSignature, 0, rawSignedData.length);

			PdfDictionary update = new PdfDictionary();
			update.put(PdfName.CONTENTS, new PdfString(paddedSignature).setHexWriting(true));
			sap.close(update);

			baos.writeTo(os);
			baos.close();

			ResultDTO resultDTO = new ResultDTO();
			resultDTO.setPdfFileName(action.getNewPdfFileName());
			return resultDTO;
		}
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}

	public Certificate[] getCertificateChain() {
		return certificateChain;
	}

	public void setCertificateChain(Certificate[] certChain) {
		this.certificateChain = certChain;
	}

	public boolean isExternalSigning() {
		return externalSigning;
	}

	public void setExternalSigning(boolean externalSigning) {
		this.externalSigning = externalSigning;
	}

	protected static void checkCertificateUsage(X509Certificate x509Certificate) throws CertificateParsingException {
		// Check whether signer certificate is "valid for usage"
		// https://stackoverflow.com/a/52765021/535646
		// https://www.adobe.com/devnet-docs/acrobatetk/tools/DigSig/changes.html#id1
		boolean[] keyUsage = x509Certificate.getKeyUsage();
		if (keyUsage != null && !keyUsage[0] && !keyUsage[1]) {
			// (unclear what "signTransaction" is)
			// https://tools.ietf.org/html/rfc5280#section-4.2.1.3
			LOG.error("Certificate key usage does not include " + "digitalSignature nor nonRepudiation");
		}
		List<String> extendedKeyUsage = x509Certificate.getExtendedKeyUsage();
		if (extendedKeyUsage != null && !extendedKeyUsage.contains(KeyPurposeId.id_kp_emailProtection.toString())
				&& !extendedKeyUsage.contains(KeyPurposeId.id_kp_codeSigning.toString())
				&& !extendedKeyUsage.contains(KeyPurposeId.anyExtendedKeyUsage.toString())
				&& !extendedKeyUsage.contains("1.2.840.113583.1.1.5") &&
				// not mentioned in Adobe document, but tolerated in practice
				!extendedKeyUsage.contains("1.3.6.1.4.1.311.10.3.12")) {
			LOG.error("Certificate extended key usage does not include "
					+ "emailProtection, nor codeSigning, nor anyExtendedKeyUsage, "
					+ "nor 'Adobe Authentic Documents Trust'");
		}
	}

}
