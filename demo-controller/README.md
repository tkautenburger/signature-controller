# SPI PDF Document Signature Service

This service provides a RESTful API for the signature of PDF documents. The service supports the following features:

- OAuth2 JWT authentication
- Role-based user/client authorization
- CORS support
- HTTP security header support
- TLS transport security

## Configuration

All relevant configuration parameters can be set as environment parameters. The following list gives a complete overview of the environment parameters:

| Parameter                                 | Default Value                                                | Description                                                  |
| ----------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| SPI_SIGN_KEYSTORE_PATH                    | keystore.jks                                                 | The JKS keystore where the server's private key and public key certificate is stored |
| SPI_SIGN_KEYSTORE_PASSWORD                | changeit                                                     | The keystore password                                        |
| SPI_SIGN_SSL_PORT                         | 9443                                                         | the API's SSL port                                           |
| SPI_SIGN_PORT                             | 9080                                                         | the API's unsecured port (disabled for production systems)   |
| SPI_SIGN_ALLOWED_ORIGINS                  | N/A                                                          | the url pattern (regular expression) for allowed CORS origins |
| SPI_SIGN_HEADER_CONTENT_TYPE_OPTIONS      | nosniff                                                      | HTTP security header, change only if you know what you are doing |
| SPI_SIGN_HEADER_FRAME_OPTIONS             | deny                                                         | HTTP security header, change only if you know what you are doing |
| SPI_SIGN_HEADER_XSS_PROTECTION            | 1; mode=block                                                | HTTP security header, change only if you know what you are doing |
| SPI_SIGN_HEADER_CONTENT_SECURITY_POLICY   | frame-ancestors 'self'; script-src 'self' 'unsafe-inline' 'unsafe-eval'; object-src 'self'; base-uri 'self'; | HTTP security header, change only if you know what you are doing |
| SPI_SIGN_HEADER_CACHE_CONROL              | no-source, no-cache                                          | HTTP security header, change only if you know what you are doing |
| SPI_SIGN_HEADER_STRICT_TRANSPORT-SECURITY | max-age=31536000; includeSubDomains                          | HTTP security header, change only if you know what you are doing |
| SPI_SIGN_ROLE_CLAIM                       | resource                                                     | resource: look for resource-based roles in access token<br />realm: look for realm-based roles in access token |
| SPI_SIGN_ROLE_PROVIDER_CLIENT             | player-ui                                                    | The client id of the resource that contains the roles to verify (for resource-level access) |
| SPI_SIGN_ROLE_PATTERN                     | SPI-Approver                                                 | The functional component of the authorizing role to perform document signing |
| SPI_SIGN_OAUTH_ISSUER                     | N/A                                                          | The base URL of the OAuth2 authentication server, e.g. https://host:port/keycloak/auth/realms/SPI. Further OAuth2 info is retrieved fromt he server's discovery endpoint. |
| SPI_SIGN_LOG_LEVEL                        | debug                                                        | The log level of the service. For production environments the log level should be set to `info`. |

## API Endpoints

The service has only one endpoint that signs a PDF docjment. The payload of the POST request encompasses the followind data. The user must have a valid JWT token.
```
{
    "action": "sign"
    "pdfFileName": "<path of the unsigned PDF file to sign>"
    "newPdfFileName": "<path of the signed PDF file>"
    "reason": "<short signature reason displayed in the document signature>"
}
```

```
curl -k -XPOST https://<hostname>:<port>/api/sign \
     -H "content-type: application/json" \
     -H "Authorization: bearer ey......" \
     -d '{"action": "sign", "pdfFileName": "unsignedReport.pdf", "newPdfFileName": "signedReport.pdf", "reason": "Report Approval"}' | jq
```